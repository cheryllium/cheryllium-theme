<!DOCTYPE html>
<html>
  <head>
    <title>{{ config('blogfrog.site_name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="{{ asset("/themes/cheryllium/css/main.css") }}">
    <link rel="stylesheet" href="{{ asset("/rainbow/css/obsidian.css") }}">
    
  </head>
  <body>

    <sidebar>
      <div id="title">
        <a href="/"><img src="{{ asset("/themes/cheryllium/imgs/logo_transparent.png") }}" class="logo" /></a>
        <a href="/"><h1>{{ config('blogfrog.site_name') }}</h1></a>
        </a>
      </div>

      <div id="links">
        @foreach ($pages as $page)
          <a href="{{ route('blogfrog.page', $page['slug']) }}">{{ $page['title'] }}</a>
        @endforeach
        <a href="{{ route('blogfrog.rss') }}">rss</a>
      </div>
    </sidebar>

    <div id="content">
      <script src="{{ asset("/themes/cheryllium/js/p5.min.js") }}"></script>

      {{ $slot }}

      <footer>
        &copy; 2022 cheshire yang | made with BLOG FROG
      </footer>
    </div>
    
    <script src="{{ asset("/rainbow/js/rainbow.min.js") }}"></script>
  </body>
</html>
