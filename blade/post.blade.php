<article>
  <a href="{{ route('blogfrog.post', $post->slug) }}">
    <h1>{{ $post->title }}</h1>
  </a>
  <div class="post-content">
    @markdown($post->content)
  </div>

  <hr />
  <div class="post-info">
    <span>Published: {{ $post->created_at }}</span>
    @if (count($post->tags) > 0)
      <span>
        - Tags:
        @foreach ($post->tags as $tag)
          <a href="{{ route('blogfrog.tagged', $tag) }}">{{ $tag }}</a>
        @endforeach
      </span>
    @endif
  </div>
</article>
